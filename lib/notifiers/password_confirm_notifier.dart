import 'package:flutter/material.dart';
import 'package:futurememories/app_style.dart';
import 'package:futurememories/ui/form/password_confirm_tf.dart';
import 'package:futurememories/ui/form/password_new_tf.dart';

class PasswordConfirmNotifier extends ChangeNotifier{

  String _password;
  String get password => _password;

  String _confirmPassword;
  String get confirmPassword => _confirmPassword;

  String _confirmedText = "Passwords do not match!";
  String get confirmedText => _confirmedText;

  Color _confirmedColor = unSelectColor;
  Color get confirmedColor => _confirmedColor;

  void setPassword(PasswordNewTextField newPasswordTextField, String password) {
    assert(newPasswordTextField != null);

    this._password = password;
    checkSimilarity();
  }

  void setConfirmPassword(PasswordConfirmTextField confirmPasswordTextField, String password){
    assert(confirmPasswordTextField != null);

    this._confirmPassword = password;
    checkSimilarity();
  }

  void checkSimilarity(){
    _confirmedText = (_password == _confirmPassword) ? 'Passwords match!' : 'Passwords do not match!';
    _confirmedColor = (_password == _confirmPassword) ? validColor: unSelectColor;
    notifyListeners();
  }

}