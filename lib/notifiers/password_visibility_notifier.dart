import 'package:flutter/material.dart';

class PasswordVisibilityNotifier extends ChangeNotifier{

  PasswordVisibilityNotifier([this.obscureText = true]);

  bool obscureText;

  void toggleVisibility() {
    obscureText = !obscureText;
    notifyListeners();
  }

}