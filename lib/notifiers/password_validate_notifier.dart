import 'package:flutter/material.dart';

class PasswordValidateNotifier extends ChangeNotifier{
  bool _isValid = false;
  bool get isValid => _isValid;

  String _helperText;
  String get helperText => _helperText;

  bool validate(String pass, regexText, String helperTextIfValid, String helperTextIfNotValid){
    bool currentValidState = _isValid;

    _isValid = RegExp(regexText).hasMatch(pass);

    _helperText = (_isValid) ? helperTextIfValid : helperTextIfNotValid;

    if(currentValidState != _isValid) {
      print(_helperText);
      notifyListeners();
    }
    return _isValid;
  }

}