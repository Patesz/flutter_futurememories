import 'package:flutter/material.dart';
import 'package:futurememories/ui/form/password_base.dart';

class _Strength {
  String _strengthText;
  double _percentage;
  Color _color;

  _Strength(this._strengthText, this._percentage, this._color);

  _Strength.veryWeak() {
    this._strengthText = "Very Weak";
    this._percentage = 0.15;
    this._color = Colors.red[800];
  }

  _Strength.weak() {
    this._strengthText = "Weak";
    this._percentage = 0.3;
    this._color = Colors.red[600];
  }

  _Strength.medium() {
    this._strengthText = "Medium";
    this._percentage = 0.5;
    this._color = Colors.yellow;
  }

  _Strength.strong() {
    this._strengthText = "Strong";
    this._percentage = 0.75;
    this._color = Colors.green;
  }

  _Strength.veryStrong() {
    this._strengthText = "Very Strong";
    this._percentage = 1;
    this._color = Colors.green[700];
  }
}

class PasswordStrengthNotifier extends ChangeNotifier{
  var _currentStrength = _Strength.veryWeak();
  var _isVisible = false;

  String get strengthText => _currentStrength._strengthText;
  double get percentage => _currentStrength._percentage;
  Color get color => _currentStrength._color;
  bool get isVisible => _isVisible;

  void changePasswordStrength(String passwordText) {
      _isVisible = (passwordText.length == 0) ? false : true;

      String currentStrengthText = _currentStrength._strengthText;

      double point = passwordText.length.toDouble();

      point = RegExp(lowerRegexText).hasMatch(passwordText) ? point *= 1.1 : point;
      point = RegExp(upperRegexText).hasMatch(passwordText) ? point *= 1.2 : point;
      point = RegExp(numberRegexText).hasMatch(passwordText) ? point *= 1.2 : point;
      point = RegExp(specialRegexText).hasMatch(passwordText) ? point *= 1.4 : point;

      if (point > 25) {
        _currentStrength = _Strength.veryStrong();
      } else if (point > 16) {
        _currentStrength = _Strength.strong();
      } else if (point > 12) {
        _currentStrength = _Strength.medium();
      } else if (point > 6) {
        _currentStrength = _Strength.weak();
      } else {
        _currentStrength = _Strength.veryWeak();
      }

      // Only notify listeners if currentStrength changed instead of every character type
      if(currentStrengthText != _currentStrength._strengthText || _isVisible == false || passwordText.length - 1 == 0) {
        notifyListeners();
      }
  }

}