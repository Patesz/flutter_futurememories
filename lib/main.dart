import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'ui/route/login.dart';
import './app_style.dart';

void main() => runApp(AppEntry());

class AppEntry extends StatelessWidget {
  final primarySwatch = primaryColor;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: Key("App"),
      title: 'Future Memories',
      // Do NOT use darkTheme: ThemeData.light() or .dark(). Instead define brightness param in theme: ThemeData
      debugShowCheckedModeBanner: true,
      // show/hide DEBUG stripe on top-right corner
      theme: ThemeData(
        primarySwatch: primarySwatch,
        visualDensity: VisualDensity.adaptivePlatformDensity,

        /*brightness: Brightness.dark,
        primaryColor: Colors.red,
        accentColor: Colors.redAccent,
        textTheme: TextTheme(bodyText2: TextStyle(color: Colors.white)),*/
      ),
      home: LogInPage(
        Key("LogInPage"),
        'Sign in Page',
      ),
    );
  }
}
