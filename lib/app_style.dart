import 'package:flutter/material.dart';

final primaryDarkColor = Colors.blue[800];
const primaryColor = Colors.blue;
final secondaryColor = Colors.blue[400];

const whiteTextColor = Colors.white;
final brightTextColor = Colors.grey[100];

final darkTextColor = Colors.grey[900];
const blackTextColor = Colors.black87;

final unSelectColor = Colors.grey[500];

final validColor = Colors.green;
final errorColor = Colors.red;