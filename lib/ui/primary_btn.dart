import 'package:flutter/material.dart';

import 'package:futurememories/app_style.dart';

class PrimaryButton extends StatelessWidget {

  final String text;
  final Color color;
  final Color textColor;
  final Function onPressedCallback;

  PrimaryButton(this.text, {this.color = primaryColor, this.textColor = whiteTextColor, this.onPressedCallback})
      : assert(text != null);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 25.0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(30.0),
          color: color,
          child: MaterialButton(
            minWidth: MediaQuery
                .of(context)
                .size
                .width,
            padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            onPressed: () {
              this.onPressedCallback();
            },
            child: Text(this.text,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: textColor,
                fontSize: 16,
              )
            ),
          ),
        )
    );
  }
}