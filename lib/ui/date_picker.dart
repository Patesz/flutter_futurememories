import 'package:flutter/material.dart';

class DatePicker extends StatefulWidget {

  final DateTime _initialDate;
  final DateTime _firstDate;
  final DateTime _lastDate;

  DatePicker(this._initialDate, this._firstDate, this._lastDate);

  @override
  _DatePickerState createState() => _DatePickerState();

}

class _DatePickerState extends State<DatePicker> {

  var selectedDate;

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: widget._initialDate,
        firstDate: widget._firstDate,
        lastDate: widget._lastDate,
    );
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () => _selectDate(context),
      child: Text('Select date: $selectedDate'),
    );
  }
}