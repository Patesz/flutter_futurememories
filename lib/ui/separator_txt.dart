import 'package:flutter/material.dart';

import 'package:futurememories/app_style.dart';

class SeparatorText extends StatelessWidget {
  final String text;
  final Color dividerColor;
  final double dividerSideMargin;

  SeparatorText(this.text, this.dividerColor, [this.dividerSideMargin = 10.0])
      : assert(text != null, dividerSideMargin != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 42.0, bottom: 10.0, left: 10.0, right: 10.0),
      child: Row(
        children: <Widget>[
          Flexible(
            child: Container(
              margin: EdgeInsets.only(right: dividerSideMargin),
              child: Divider(
                color: dividerColor,
                thickness: 1.5,
              ),
            ),
          ),
          Text(text),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: dividerSideMargin),
              child: Divider(
                color: dividerColor,
                thickness: 1.5,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
