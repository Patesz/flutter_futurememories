import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:futurememories/notifiers/password_strength_notifier.dart';
import 'package:provider/provider.dart';

class PasswordStrengthIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PasswordStrengthNotifier passwordStrengthBloc =
        Provider.of<PasswordStrengthNotifier>(context);

    return Visibility(
      /*maintainSize: true,
        maintainAnimation: true,
        maintainState: true,*/
      visible: passwordStrengthBloc.isVisible,
      child: Container(
        margin: EdgeInsets.all(5.0),
        child: Column(children: <Widget>[
          LinearProgressIndicator(
            value: passwordStrengthBloc.percentage,
            backgroundColor: Colors.white,
            valueColor: new AlwaysStoppedAnimation<Color>(
                passwordStrengthBloc.color
            ),
          ),
          Text(
            passwordStrengthBloc.strengthText,
            textAlign: TextAlign.left,
            style: TextStyle(
              color: Colors.black,
              fontSize: 10,
            ),
          ),
        ]),
      ),
    );
  }
}
