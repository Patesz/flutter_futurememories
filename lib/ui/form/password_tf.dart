import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:futurememories/app_style.dart';
import 'package:futurememories/notifiers/password_visibility_notifier.dart';
import 'package:futurememories/ui/form/password_base.dart';
import 'file:///C:/Users/Ricky/Documents/Programming/Flutter/future_memories/lib/ui/show_help.dart';
import 'package:provider/provider.dart';

class PasswordTextField extends Password {
  PasswordTextField()
      : super(
          "Password",
          helperText: "Enter password",
        );

  @override
  Widget build(BuildContext context) {
    final passwordVisibilityBloc = Provider.of<PasswordVisibilityNotifier>(context);

    return Container(
      margin: super.containerEdgeInsets,
      child: TextFormField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(super.textLengthLimit),
          ],
          validator: (String pass) {
            bool passValid = RegExp(passwordRegexText).hasMatch(pass);
            if (pass.isEmpty) {
              return '*Required';
            }
            return passValid ? null : 'Password is not valid!';
          },
          keyboardType: super.textInputType,
          obscureText: passwordVisibilityBloc.obscureText,
          decoration: buildPasswordInputDecoration(
            passwordVisibilityBloc.obscureText,
            passwordVisibilityBloc.toggleVisibility,
          )
        ),
      );
  }
}
