import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:futurememories/app_style.dart';

const emailRegexText = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";

class EmailTextField extends StatefulWidget {

  EmailTextField({Key key}) : super(key: key);

  @override
  _EmailTextFieldState createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  var _characterCounter = 0;
  var _helperText = "Enter your email";
  var _color = unSelectColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: TextFormField(
        inputFormatters: [
          new LengthLimitingTextInputFormatter(254),
        ],
        onChanged: (text) {
          setState(() {
            _characterCounter = text.length;

            bool emailValid = RegExp(emailRegexText).hasMatch(text);
            _helperText = emailValid ? 'Email seems valid' : 'Enter your email';
            _color = emailValid ? validColor : unSelectColor;
          });
        },
        keyboardType: TextInputType.emailAddress,
        validator: (String email) {
          bool emailValid = RegExp(emailRegexText).hasMatch(email);
          if(email.isEmpty){
            return '*Required';
          }
          return emailValid ? null : 'Email is not valid!';
        },
        decoration: InputDecoration(
          // Other icons: email, account_box, account_circle, alternate_email
          prefixIcon: Icon(Icons.account_circle),
          hintText: 'Email',
          helperText: _helperText,
          helperStyle: TextStyle(
              color: _color,
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            // Change this to green if the field is valid!
            borderSide: new BorderSide(
              color: _color,
              width: 1.0,
            ),
          ),
          counterText: '$_characterCounter characters',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }
}
