import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:futurememories/app_style.dart';
import 'package:futurememories/notifiers/password_confirm_notifier.dart';
import 'package:futurememories/notifiers/password_strength_notifier.dart';
import 'package:futurememories/notifiers/password_validate_notifier.dart';
import 'package:futurememories/notifiers/password_visibility_notifier.dart';
import 'package:futurememories/ui/form/password_base.dart';
import 'file:///C:/Users/Ricky/Documents/Programming/Flutter/future_memories/lib/ui/show_help.dart';
import 'package:provider/provider.dart';

class PasswordNewTextField extends Password {

  PasswordNewTextField()
      : super('New password',
        helperText: 'Enter new password!',);

  @override
  Widget build(BuildContext context) {
    final passwordStrengthBloc = Provider.of<PasswordStrengthNotifier>(context);
    final passwordVisibilityBloc = Provider.of<PasswordVisibilityNotifier>(context);
    final passwordValidateBloc = Provider.of<PasswordValidateNotifier>(context);
    final passwordConfirmBloc = Provider.of<PasswordConfirmNotifier>(context);

    return Container(
      margin: super.containerEdgeInsets,
        child: TextFormField(
          inputFormatters: [
            LengthLimitingTextInputFormatter(super.textLengthLimit),
          ],
          onChanged: (password) {
            passwordStrengthBloc.changePasswordStrength(password);
            passwordValidateBloc.validate(password, passwordRegexText, 'Password is valid!', 'Password must be at least 8 character, contain a small, capital and a number character!', );
            passwordConfirmBloc.setPassword(this, password);
          },
          validator: (String pass) {
            bool passValid = RegExp(passwordRegexText).hasMatch(pass);
            if (pass.isEmpty) {
              return '*Required';
            }
            return passValid ? null : 'Password is not valid!';
          },
          keyboardType: super.textInputType,
          obscureText: passwordVisibilityBloc.obscureText,

          decoration: buildPasswordInputDecoration(
              //passwordErrorBloc.error,
              passwordVisibilityBloc.obscureText,
              passwordVisibilityBloc.toggleVisibility,
              newHelperText: passwordValidateBloc.helperText,
              validColor: passwordValidateBloc.isValid ? validColor : unSelectColor),

        ),
    );
  }

}
