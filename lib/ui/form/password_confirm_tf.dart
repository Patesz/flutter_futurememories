import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:futurememories/app_style.dart';
import 'package:futurememories/notifiers/password_confirm_notifier.dart';
import 'package:futurememories/notifiers/password_visibility_notifier.dart';
import 'package:futurememories/ui/form/password_base.dart';
import 'file:///C:/Users/Ricky/Documents/Programming/Flutter/future_memories/lib/ui/show_help.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class PasswordConfirmTextField extends Password {

  PasswordConfirmTextField(
  ) : super(
          "Confirm Password",
           helperText: "Enter same password as above!",
        );

  @override
  Widget build(BuildContext context) {
    final passwordVisibilityBloc = Provider.of<PasswordVisibilityNotifier>(context);
    final passwordConfirmBloc = Provider.of<PasswordConfirmNotifier>(context);

    return Container(
      margin: super.containerEdgeInsets,
      child: TextFormField(
        inputFormatters: [
          LengthLimitingTextInputFormatter(super.textLengthLimit),
        ],
        onChanged: (String password) {
          passwordConfirmBloc.setConfirmPassword(this, password);
        },
        validator: (String pass) {
          if (pass.isEmpty) {
            return '*Required';
          } else if (passwordConfirmBloc.password != pass) {
            return 'Passwords do not match!';
          } else {
            return null;
          }
        },
        keyboardType: super.textInputType,
        obscureText: passwordVisibilityBloc.obscureText,
        decoration: buildPasswordInputDecoration(
            passwordVisibilityBloc.obscureText,
            passwordVisibilityBloc.toggleVisibility,
            newHelperText: passwordConfirmBloc.confirmedText,
            validColor: passwordConfirmBloc.confirmedColor),
      ),
    );
  }
}
