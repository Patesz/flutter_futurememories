import 'package:flutter/material.dart';

import 'package:futurememories/app_style.dart';

class ForgotPasswordText extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      margin: const EdgeInsets.only(),
      child: FlatButton(
        onPressed: () { },
        child: Text(
          "Forgot password?",
          style: TextStyle(
            decoration: TextDecoration.underline,
            color: primaryDarkColor,
          ),
        ),
      ),
    );
  }

}
