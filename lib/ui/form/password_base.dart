import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

import 'package:futurememories/app_style.dart';

const numberRegexText = "(?=.*[0-9])";
const lowerRegexText = "(?=.*[a-z])";
const upperRegexText = "(?=.*[A-Z])";
const specialRegexText = "(?=.*[\$@+\\-,_~{}()|/=`:;'\"!%*#?&])";

const passwordRegexText = "^" +
    numberRegexText // Must contain at least one number
    +
    lowerRegexText // ... one lowercase character
    +
    upperRegexText // ... one capital character
    //+
    //specialRegexText // ... one special character   "(?=.*[@#$%^&+=])"    /_~{}[]()=`:;'\"
    +
    ".{8,256}\$"; // password length must be between the interval


// ignore: must_be_immutable
abstract class Password extends StatelessWidget {

  final TextInputType textInputType = TextInputType.visiblePassword;
  final EdgeInsets containerEdgeInsets = const EdgeInsets.only(top: 10.0, bottom: 10.0);
  final int textLengthLimit;

  final String hintText;
  final String helperText;
  final String labelText;

  final IconData prefixIcon;

  /*set setHelperText(String text){
    this.helperText = text;
  }*/

  Password(this.hintText,
      {this.helperText, this.labelText, this.textLengthLimit = 255, this.prefixIcon = Icons.lock_outline,})
      : assert (textLengthLimit != null && textLengthLimit >= 16);

  InputDecoration buildPasswordInputDecoration(/*bool error,*/ bool isObscureText, Function toggleVisibility, {String newHelperText, Color validColor}){
    //assert(error != null);
    /*if(error == false){
      print("No error");
    } else if(error == true) {
      print("Error is here");
    } else {
      print("Error is null");
    }*/
    //print("Error: " + error);
    return InputDecoration(
      helperMaxLines: 9999999,
      hintText: hintText,
      helperText: (newHelperText == null) ? this.helperText : newHelperText,
      helperStyle: TextStyle(
        color: (validColor == null) ? unSelectColor : validColor
      ),
      labelText: labelText,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        // Change this to green if the field is valid!
        borderSide: new BorderSide(
          color: (validColor == null) ? unSelectColor : validColor,
          width: 1.0,
        ),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      //lock, lock_outline, vpn_key
      prefixIcon: Icon(prefixIcon),
      suffixIcon: IconButton(
        splashColor: primaryColor,
        onPressed: toggleVisibility,
        icon: Icon(
          isObscureText ? Icons.visibility_off : Icons.visibility,
        ),
      ),
    );
  }

}