import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputFormField extends StatelessWidget {

  final String _hintText;
  final String _helperText;
  final int _maxLength;
  final double _rightMargin;
  final double _leftMargin;
  final Function _validator;

  InputFormField(this._hintText, {helperText, maxLength = 255, leftMargin = 0.0, rightMargin = 0.0, validator})
      : _helperText = helperText, _maxLength = maxLength, _leftMargin = leftMargin, _rightMargin = rightMargin, _validator = validator,
      assert(maxLength != null), assert(leftMargin != null), assert(rightMargin != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 10.0, left: _leftMargin, right: _rightMargin),
      child: TextFormField(
        inputFormatters: [
          new LengthLimitingTextInputFormatter(_maxLength),
        ],
        validator: this._validator,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: _hintText,
          helperText: _helperText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
        ),
      ),
    );
  }
}

