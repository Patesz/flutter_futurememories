import 'package:flutter/material.dart';

import 'package:futurememories/app_style.dart';

class ShowHelp extends StatefulWidget {

  final String _help;
  final bool _initVisibility;
  final double _textSize;

  ShowHelp(this._help, [this._initVisibility = false, this._textSize]);

  @override
  _ShowHelpState createState() => _ShowHelpState(_initVisibility);
}

class _ShowHelpState extends State<ShowHelp> {
  bool _isVisible;
  bool get getIsVisible{
    return _isVisible;
  }

  _ShowHelpState(this._isVisible);

  void _toggleVisibility(){
    _isVisible = !_isVisible;
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
        visible: _isVisible,
        child: Text(
            widget._help,
          style: TextStyle(
            fontSize: widget._textSize
          ),
        ),
    );
  }
}