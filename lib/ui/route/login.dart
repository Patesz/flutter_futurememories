import 'package:flutter/material.dart';

import 'package:futurememories/app_style.dart';
import 'package:futurememories/notifiers/password_visibility_notifier.dart';
import 'package:futurememories/ui/form/email_tf.dart';
import 'package:futurememories/ui/form/forgot_password_txt.dart';
import 'package:futurememories/ui/form/password_tf.dart';

import 'package:futurememories/ui/route/register.dart';

import 'package:provider/provider.dart';

import '../primary_btn.dart';
import '../separator_txt.dart';

class LogInPage extends StatelessWidget {
  final String _title;
  final _loginFormKey = GlobalKey<FormState>();

  LogInPage(Key key, this._title) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PasswordVisibilityNotifier>.value(
          value: PasswordVisibilityNotifier(),
        ),
      ],
      child: Scaffold(
          // use resizeToAvoidBottomPadding: false to avoid pixel overflow error OR SingleChildScrollView widget under scaffold
          //resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text(this._title),
          ),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.all(20.0),
                child: Form(
                  key: _loginFormKey,
                  child: Column(
                    // Use mainAxisAlignment to center the children vertically
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      EmailTextField(),
                      PasswordTextField(),
                      ForgotPasswordText(),
                      PrimaryButton(
                        "Log in",
                        color: primaryColor,
                        textColor: whiteTextColor,
                        onPressedCallback: (() => {
                              if (_loginFormKey.currentState.validate())
                                {
                                  // If the form is valid, display a Snackbar.
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text('Processing Data')))
                                }
                            }),
                      ),
                      SeparatorText("or", unSelectColor),
                      PrimaryButton(
                        "Register",
                        color: whiteTextColor,
                        textColor: darkTextColor,
                        onPressedCallback: (() => {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegisterPage()),
                              )
                            }),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
