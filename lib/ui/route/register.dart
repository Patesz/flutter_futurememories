import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:futurememories/notifiers/password_confirm_notifier.dart';
import 'package:futurememories/notifiers/password_strength_notifier.dart';
import 'package:futurememories/notifiers/password_validate_notifier.dart';
import 'package:futurememories/notifiers/password_visibility_notifier.dart';

import 'package:futurememories/app_style.dart';
import 'package:futurememories/ui/form/email_tf.dart';
import 'package:futurememories/ui/form/input_tf.dart';
import 'package:futurememories/ui/form/password_confirm_tf.dart';
import 'package:futurememories/ui/form/password_new_tf.dart';
import 'package:futurememories/ui/form/password_strength_indicator.dart';
import 'package:provider/provider.dart';

import '../primary_btn.dart';

class RegisterPage extends StatelessWidget {
  final _registerFormKey = GlobalKey<FormState>();

  String validateName(String name) {
    return (name.length == 0)
        ? '*Required'
        : (name.length < 2) ? 'Name is too short!' : null;
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PasswordStrengthNotifier>.value(
          value: PasswordStrengthNotifier(),
        ),
        ChangeNotifierProvider<PasswordVisibilityNotifier>.value(
          value: PasswordVisibilityNotifier(),
        ),
        ChangeNotifierProvider<PasswordValidateNotifier>.value(
          value: PasswordValidateNotifier(),
        ),
        ChangeNotifierProvider<PasswordConfirmNotifier>.value(
          value: PasswordConfirmNotifier(),
        ),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: Text("Register Page"),
        ),
        body: Center(
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent.
          child: SingleChildScrollView(
            child: Container(
              margin: const EdgeInsets.all(20.0),
              child: Form(
                autovalidate: true,
                key: _registerFormKey,
                child: Column(
                  // Use mainAxisAlignment to center the children vertically
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Flexible(
                        child: InputFormField(
                          "First name",
                          helperText: "Enter your first name",
                          maxLength: 50,
                          rightMargin: 8.0,
                          validator: validateName,
                        ),
                      ),
                      Flexible(
                        child: InputFormField("Last name",
                            helperText: "Enter your last name",
                            maxLength: 50,
                            leftMargin: 8.0,
                            validator: validateName),
                      ),
                    ]),
                    EmailTextField(),
                    PasswordNewTextField(),
                    Container(
                      margin:
                          EdgeInsets.only(left: 12.0, right: 12.0, bottom: 5.0),
                      child: Text(
                        '''Password must contain an upper and a lowercase character with a number and it must be at least 8 character!
(Optional) Use special characters to make it even stronger.''',
                        style: TextStyle(
                          fontSize: 12,
                          color: unSelectColor,
                        ),
                      ),
                    ),
                    PasswordStrengthIndicator(),
                    PasswordConfirmTextField(),
                    PrimaryButton("Register",
                        color: primaryColor,
                        textColor: whiteTextColor,
                        onPressedCallback: (() => {

                        })),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
