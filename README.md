# Future Memories

Media storage application for user groups (e.g. families).

Main functionalities:
- User registration, login and verification
- Picking file from local device, caching, cloud uploading (low/high resolution).
- Media viewer
- Album management

**Front-end:** Flutter

**Back-end:** Firebase (Authentication, Firestore, Storage)

**Project phase:** Development
